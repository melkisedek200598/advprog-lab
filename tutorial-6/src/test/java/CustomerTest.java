import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class CustomerTest {
    Customer customer;

    @Before
    public void setUp() {
        this.customer = new Customer("Alice");
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        Rental rent = new Rental(movie, 3);
        customer.addRental(rent);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!
        Movie movie = new Movie("Legend", Movie.NEW_RELEASE);
        Rental rent = new Rental(movie, 10);
        customer.addRental(rent);
        String result = customer.statement();
        assertTrue(result.contains("Amount owed is 33.5"));
        assertTrue(result.contains("3 frequent renter points"));

        movie = new Movie("The Flash", Movie.CHILDREN);
        rent = new Rental(movie, 5);
        customer.addRental(rent);
        result = customer.statement();
        assertTrue(result.contains("Amount owed is 38.0"));
        assertTrue(result.contains("4 frequent renter points"));
    }

    @Test
    public void statementInHtml() {
        String expected = "<H1>Rentals for"
                + " <EM>Alice</EM></ H1><P>\n"
                + "Who Killed Captain Alex?: 3.5<BR>\n"
                + "<P>You owe <EM>3.5</EM><P>\n"
                + "On this rental you earned "
                + "<EM>1</EM> frequent renter points<P>";
        assertEquals(expected, customer.htmlStatement());
    }

}
