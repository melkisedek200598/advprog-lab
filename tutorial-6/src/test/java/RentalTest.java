import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {
    Movie movie;
    Rental rent;

    @Before
    public void setUp() {
        this.movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        this.rent = new Rental(movie, 3);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
        assertFalse(movie.equals(null));
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

}
