package sorting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    private static String pathFile = "plainTextDirectory/input/sortingProblem.txt";
    private static int numberOfItemToBeSorted = 50000;

    public static void main(String[] args) throws IOException {

        int[] sequenceInput = convertInputFileToArray();

        //Searching Input Before Sorting
        double totalMilisSearchBeforeSort = System.nanoTime() * 1E-6;
        int searchingResultBeforeSort = Finder.slowSearch(sequenceInput, 40738);
        totalMilisSearchBeforeSort = System.nanoTime() * 1E-6 - totalMilisSearchBeforeSort;
        System.out.println("Searching Complete in " + totalMilisSearchBeforeSort + " milisecond");

        //Sorting Input
        double totalMilisSorting = System.nanoTime() * 1E-6;
        int[] sortedInput = Sorter.quickSort(sequenceInput, 0,
                sequenceInput.length - 1);
        totalMilisSorting = System.nanoTime() * 1E-6 - totalMilisSorting;
        System.out.println("Sorting Complete in " + totalMilisSorting + " milisecond");

        //Searching Input After Sorting
        double totalMilisSearchAfterSort = System.nanoTime() * 1E-6;
        int searchingResultAfterSort = Finder.binarySearch(sequenceInput, 40738);
        totalMilisSearchAfterSort = System.nanoTime() * 1E-6 - totalMilisSearchAfterSort;
        System.out.println("Searching Complete in " + totalMilisSearchAfterSort + " milisecond");
    }

    /**
     * Converting a file input into an array of integer.
     *
     * @return an array of integer that represent an integer sequence.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    private static int[] convertInputFileToArray() throws IOException {
        File sortingProblemFile = new File(pathFile);
        FileReader fileReader = new FileReader(sortingProblemFile);
        int[] sequenceInput = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            sequenceInput[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
        return sequenceInput;
    }
}
