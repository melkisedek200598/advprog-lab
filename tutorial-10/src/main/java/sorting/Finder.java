package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     *
     * @param arrOfInt      is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */
    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    public static int binarySearch(int[] arrOfInt, int searchedValue) {
        int l = 0;
        int r = arrOfInt.length - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (arrOfInt[mid] == searchedValue) {
                return mid;
            } else if (arrOfInt[mid] > searchedValue) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return -1;
    }


}
