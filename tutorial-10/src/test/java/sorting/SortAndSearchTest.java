package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import static sorting.Sorter.quickSort;
import static sorting.Sorter.slowSort;

import static sorting.Finder.binarySearch;
import static sorting.Finder.slowSearch;

import org.junit.Test;

public class SortAndSearchTest {
    public boolean isArraySortedAscending(int[] arr) {
        for (int i = 1; i < arr.length; ++i) {
            if (arr[i] < arr[i - 1]) return false;
        }
        return true;
    }

    @Test
    public void slowSortShouldReturnAscendingSortedArray() {
        int arr[] = {1, -1, -2, 10000, 5, 8, 9, -10};
        assertTrue(isArraySortedAscending(slowSort(arr)));
    }

    @Test
    public void mergeSortShouldReturnAscendingSortedArray() {
        int arr[] = {-1, 5, -10, 100000, 71, 3, -2, -4};
        assertTrue(isArraySortedAscending(quickSort(arr, 0, arr.length - 1)));
    }

    @Test
    public void slowSearchShouldReturnCorrect() {
        int arr[] = {1, 2, 10, 3, 4};
        assertEquals(slowSearch(arr, 10), 10);
    }

    @Test
    public void binarySearchShouldReturnCorrectIndexIfTheInputIsSorted() {
        int arr[] = {1, 2, 3, 101, 102, 103};
        assertEquals(5, binarySearch(arr, 103));
    }

    @Test
    public void binarySearchShouldReturnWrongIndexIfInputIsNotSorted() {
        int arr[] = {10000, 2013, 2014, -1, 1, 0};
        assertNotEquals(3, binarySearch(arr, -1));
    }

}
