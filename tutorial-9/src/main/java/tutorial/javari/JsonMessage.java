package tutorial.javari;

public class JsonMessage {
    private String messageType;
    private String message;

    public JsonMessage(String messageType, String message) {
        this.messageType = messageType;
        this.message = message;
    }

    public JsonMessage notFoundMessage(int idAnimal) {
        return new JsonMessage("Unknown",
                "Sorry, there is no animal with id no." + idAnimal);
    }

    public JsonMessage nothingInDatabase() {
        return new JsonMessage("Error",
                "Sorry, our database is empty");
    }

    public JsonMessage successDeleteAnimal() {
        return new JsonMessage("Success",
                "The animal is successfully deleted from the database."
                        + "This is the data from the animal that just got deleted:");
    }

    public JsonMessage successAddAnimal() {
        return new JsonMessage("Success",
                "The animal successfully added to the database."
                        + "This is the data from the animal that just got added:");
    }

    public String getMessageType() {
        return messageType;
    }

    public String getMessage() {
        return message;
    }


}
