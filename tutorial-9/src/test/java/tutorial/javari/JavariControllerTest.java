package tutorial.javari;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tutorial.javari.animal.Animal;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {
    // TODO Implement me! (additional task)
    @Autowired
    private MockMvc mockMvc;
    private Animal testerAnimal;
    private JavariDatabase database;
    private String testerJson;
    private int testerId;

    @Before

    public void setUp() throws IOException {
        database = new JavariDatabase();
        testerId = 5;
        testerAnimal = database.getAnimalwithId(testerId);
        testerJson = convertAnimalintoJson(testerAnimal);
    }

    public String convertAnimalintoJson(Animal animal) throws JSONException {
        return new JSONObject().put("id", animal.getId())
                .put("type", animal.getType())
                .put("name", animal.getName())
                .put("gender", animal.getGender())
                .put("length", animal.getLength())
                .put("weight", animal.getWeight())
                .put("condition", animal.getCondition())
                .toString();
    }

    @Test
    public void getListAllAnimalsTest() throws Exception {
        this.mockMvc.perform(get("/javari/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].id")
                        .value("2"));
    }
}
