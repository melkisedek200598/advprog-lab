package applicant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;



public class ApplicantTest {
    private Applicant applicant;
    private Predicate<Applicant> qualifiedEvaluator;
    private Predicate<Applicant> creditEvaluator;
    private Predicate<Applicant> employmentEvaluator;
    private Predicate<Applicant> criminalRecordsEvaluator;


    @Before
    public void setUp() throws Exception {
        applicant = new Applicant();
        qualifiedEvaluator = Applicant::isCredible;
        creditEvaluator = anApplicant -> anApplicant.getCreditScore() > 600;
        employmentEvaluator = anApplicant -> anApplicant.getEmploymentYears() > 0;
        criminalRecordsEvaluator = anApplicant -> !anApplicant.hasCriminalRecord();

    }

    @Test
    public void testApplicantAccepted() {
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator)));
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)));
    }

    @Test
    public void testApplicantRejected() {
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)
                .and(criminalRecordsEvaluator)));
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(employmentEvaluator)
                .and(creditEvaluator).and(criminalRecordsEvaluator)));
    }

    @Test
    public void testPrintEvaluation() throws Exception {
        Class<?> applicantClass = Class.forName("applicant.Applicant");
        Method method = applicantClass.getDeclaredMethod("printEvaluation", boolean.class);
        method.setAccessible(true);
        method.invoke(method, true);
        method.invoke(method, false);
        int methodModifiers = applicantClass.getModifiers();
        assertEquals("void", method.getGenericReturnType().getTypeName());
    }
}
