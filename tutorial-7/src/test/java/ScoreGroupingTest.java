import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScoreGroupingTest {
    private Map<String, Integer> scores;

    @Before
    public void setUp() throws Exception {

        scores = new HashMap<>();
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

    }

    @Test
    public void testGroupingBySameScores() {
        Map<Integer, List<String>> byScores = ScoreGrouping.groupByScores(scores);
        byScores.forEach((k, v) -> v.forEach(i -> assertEquals(scores.get(i), k)));
    }

}